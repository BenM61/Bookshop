import { BrowserRouter, Routes, Route } from "react-router-dom";

import Home from "./Components/Home";
import Navbar from "./Components/Navbar";
import Shop from "./Components/Shop";
import Book from "./Components/Book";
import Receipt from "./Components/Receipt";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar />
        <div className="content">
          <Routes>
            <Route index element={<Home />} />
            <Route path="/books" element={<Shop />} />
            <Route path="/books/:id" element={<Book />} />
            <Route path="/payment" element={<Receipt />} />
          </Routes>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
