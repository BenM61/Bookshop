import { Link } from "react-router-dom";

const BookPreview = (props) => {
  const { book, id } = props;
  const path = book.image;

  return (
    <div className="book-preview">
      <Link to={`/books/${id}`} state={{ book }}>
        <img src={path} alt={book.title} />
      </Link>
    </div>
  );
};

export default BookPreview;
