import BookList from "./BookList";

const Shop = (props) => {
  return (
    <div className="shop page">
      <BookList />
    </div>
  );
};

export default Shop;
