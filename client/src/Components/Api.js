import axios from "axios";

const BaseUrl = "http://localhost:7000";

const getBookPage = async (pageNum) =>
  axios.get(`${BaseUrl}/books/page/${pageNum}`);
const postReceipt = async (date, total, description) =>
  axios.post(`${BaseUrl}/Receipts/add`, { date, total, description });

export { getBookPage, postReceipt };
