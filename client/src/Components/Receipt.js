import { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { useRecoilState } from "recoil";

import { postReceipt } from "./Api";
import { selectedBooksState } from "./Atoms";

const Receipt = () => {
  const navigate = useNavigate();
  const [selectedBooks, setSelectedBooks] = useRecoilState(selectedBooksState);
  const [purchaseComplete, setPurchaseComplete] = useState(false);
  const [receiptBooks, setReceiptBooks] = useState([]);

  useEffect(() => {
    if (!purchaseComplete) {
      setReceiptBooks(selectedBooks);
    }
  }, [selectedBooks]);

  const priceOfAll = () => {
    const total = receiptBooks.reduce(
      (prevValue, currBook) =>
        prevValue + parseFloat(currBook.price) * currBook.amount,
      0
    );
    return total.toFixed(2);
  };

  const isValidPurchase = () => {
    return selectedBooks.length > 0;
  };

  const handle_purchase = async () => {
    const date = new Date();
    const curr_date = `${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`;
    const total = priceOfAll();
    const description = selectedBooks.map(({ _id, title, price, amount }) => ({
      _id,
      title,
      price,
      amount,
    }));

    postReceipt(curr_date, total, description);
    setPurchaseComplete(true);
    setSelectedBooks([]);
  };

  const removeItem = (_id) => {
    setSelectedBooks((currBooks) =>
      currBooks.filter((book) => book._id !== _id)
    );
  };

  return (
    <div className="receipt page">
      <div className="receipt-preview">
        {receiptBooks.map(({ _id, title, price, amount }) => {
          return (
            <div className="receipt-book" key={_id}>
              <div className="receipt-details">
                <div className="rec-title">{title}: </div>
                <div className="rec-money">
                  <div className="rec-price">${price}</div>
                  <div className="rec-amount">x{amount}</div>
                </div>
              </div>
              <button
                disabled={purchaseComplete}
                onClick={() => {
                  removeItem(_id);
                }}
                className="cancel-book"
              ></button>
            </div>
          );
        })}
        <div className="total-price">
          <br />
          The total price is: ${priceOfAll()}
        </div>
        <br />

        {!purchaseComplete && !isValidPurchase() && (
          <button className="confirm" disabled onClick={handle_purchase}>
            Confirm
          </button>
        )}
        {!purchaseComplete && isValidPurchase() && (
          <button className="confirm" onClick={handle_purchase}>
            Confirm
          </button>
        )}
        {purchaseComplete && (
          <div className="thank-you">
            Thank you!
            <button
              onClick={() => {
                navigate("/");
              }}
            >
              Home
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default Receipt;
