import { atom } from "recoil";

const selectedBooksState = atom({
  key: "selectedBooks",
  default: [],
});

export { selectedBooksState };
