import { useState } from "react";
import { useLocation, Link } from "react-router-dom";
import { useSetRecoilState } from "recoil";

import { selectedBooksState } from "./Atoms";

const Book = () => {
  const setSelectedBooks = useSetRecoilState(selectedBooksState);

  const { state } = useLocation();
  const { book } = state;
  const [amount, setAmount] = useState(1);

  const handleClick = (e) => {
    book.amount = amount;
    setSelectedBooks((selectedBooks) => {
      const filtered = selectedBooks.filter(
        (listBook) => listBook._id !== book._id
      );
      return [...filtered, book];
    });
  };

  const handleAmount = (c) => {
    setAmount(c === "+" ? amount + 1 : Math.max(1, amount - 1));
  };
  return (
    <div className="book page">
      <h1>{book.title}</h1>
      <img className="book-image" src={book.image} alt={book.title}></img>
      <br />
      <div className="change-amount">
        <button onClick={() => handleAmount("-")}>-</button>
        {amount}
        <button onClick={() => handleAmount("+")}>+</button>
      </div>
      <div className="book-price">${(amount * book.price).toFixed(2)}</div>
      <Link to="/books">
        <button onClick={handleClick}>Add to cart</button>
      </Link>
    </div>
  );
};

export default Book;
