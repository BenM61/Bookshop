import BookPreview from "./BookPreview";
import { getBookPage } from "./Api";

import { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import Grid from "@mui/material/Grid";

const BookList = () => {
  const [currPageNum, setCurrPageNum] = useState(0);
  const [books, setBooks] = useState([]);
  const [hasMore, setHasMore] = useState(true);
  const [isInitializing, setIsInitializing] = useState(true);

  const getNextPage = async () => {
    const { data } = await getBookPage(currPageNum);
    setCurrPageNum(currPageNum + 1);
    setBooks((books) => books.concat(data));
    if (data.length === 0) {
      setHasMore(false);
    }
  };

  useEffect(() => {
    async function getInitialBooks() {
      await getNextPage();
      setIsInitializing(false);
    }
    getInitialBooks();

    // restore currPageNum=0 on cleanup
    return () => {
      setCurrPageNum(0);
    };
  }, []);

  return (
    <div className="book-list">
      <div className="book-list-content">
        {!isInitializing && (
          <InfiniteScroll
            dataLength={books.length}
            next={getNextPage}
            hasMore={hasMore}
          >
            <Grid container spacing={0}>
              {books.map((book) => {
                return (
                  <Grid item xs={3} key={book._id}>
                    <BookPreview book={book} id={book._id}></BookPreview>
                  </Grid>
                );
              })}
            </Grid>
          </InfiniteScroll>
        )}
      </div>
    </div>
  );
};

export default BookList;
